async function Create() {
    const email = document.getElementById('email').value;
    const name = document.getElementById('name').value;
    const password = document.getElementById('password').value;
    const role = document.getElementById('role').value;

    /* const respone = await axios.post('http://127.0.0.1:8080/api/auth/register',{
            Email: email,
            password: password,
            role: role,
            Name: name
        })
        if(respone.status == 200){
            console.log('Create success !');
        }; */
    const respone = await axios.post('http://127.0.0.1:8080/auth/admin/user/create', {
        Email: email,
        password: password,
        role: role,
        Name: name
    })
    if (respone.status == 200) {
        console.log('Create success !');
    };

}