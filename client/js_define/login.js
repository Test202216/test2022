async function handleLogin(){
    const mail = document.getElementById('email').value;
    const password = document.getElementById('password').value;

    const respone = await axios.post('http://127.0.0.1:8080/api/auth/Login',{
            Email: mail,
            password: password
        })
        if(respone.status == 200){
            const accessToken = respone.data.accessToken;
            // decode get info payload
            const payload = jwt_decode(accessToken);
            
            if(payload.role === 'regular'){
                window.location.href = '/client/home_page.html';
            }else{
                window.location.href = '/client/Admin_page.html';
            }

            localStorage.setItem('accessToken', accessToken);
        };
}