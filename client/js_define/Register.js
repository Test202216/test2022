async function Login(){
    const Name = document.getElementById('name').value;
    const Mail = document.getElementById('mail').value;
    const Password = document.getElementById('password').value;
    try{
        const respone = await axios.post('http://127.0.0.1:8080/api/auth/Login',{
            Name: Name,
            Email: Mail,
            password: Password
        })
        if(respone.status == 200){
            window.location.href = '/client/Login.html';
        };
    }
    catch(error){
        console.log(error);
    }
}