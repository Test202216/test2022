// Call API get user List
/* axios.get(`http://127.0.0.1:8080/auth/admin/user`,{
    headers:{
        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
    }
}) */
// Get List user
async function getListUser() {
    try {
        const configHeader = {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        }
        const respone = await axios.get(`http://127.0.0.1:8080/auth/admin/user`,configHeader);
        showListUser(respone);

    } catch (error) {
        console.log(error);
        if(error.response.status === 401){
            window.location.href = '/client/Login.html';   
        }
    }
}

function showListUser(respone){
    let htmlUser = `<table class="table table-hover text-nowrap">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>User</th>
                        <th>Role</th>
                        <th>Email</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>`;
    respone.data.forEach(function(user, index){
        htmlUser += `<tr>
                        <td>${index + 1}</td>
                        <td>${user.Name}</td>
                        <td>${user.role}</td>
                        <td>${user.Email}</td>
                        <td>
                            Delete
                        </td>
                    </tr>`
    });
    htmlUser += `   </tbody>
                </table>`;
    document.querySelector('.list_user').innerHTML = htmlUser;
}

getListUser();