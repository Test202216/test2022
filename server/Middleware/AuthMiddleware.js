const isAuthentication = (req, res, next) => {
    try {
        const bearerHeader = req.headers['authorization'];
        const accessToken = bearerHeader.split(' ')[1];
        const decodejwt = jwt.verify(accessToken, process.env.SECRET_JWT);
        next();
    } catch (error) {
        if(error instanceof jwt.TokenExpriedError){
            return res.status(401).send('Token Expired');
        }
        return res.status(401).send('Authentication not valid');
    }
}

module.exports = {
    isAuthentication: isAuthentication
}