const express = require('express');
const router = express.Router();
const userController = require('../Controllers/UserController');
const authMiddleware = require('../Middleware/AuthMiddleware');

router.get('/user', userController.getListUser);
router.post('/user/create', userController.postUser);

/* router.get('/detail', userController.userDetail); */

/* router.get('/', function(req,res){
    res.send('list user');
});

router.get('/details', function(req,res){
    res.send('list user details');
}); */

module.exports = router;