const express = require('express');
const router = express.Router();
/* const userController = require('../Controllers/UserController'); */
const authController = require('../Controllers/AuthController');


/* router.get('/', userController.getListUser);
router.get('/detail', userController.userDetail); */

router.post('/register',authController.register);
router.post('/Login',authController.login);

module.exports = router;