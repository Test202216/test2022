/* const http = require('http');*/

/* const server = http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end('Hello World!');
  });

server.listen(port, function(){
  console.log(`Server listen on port ${port}`);
}); */

const express = require('express')
const app = express()
const userRouter = require('./Router/UserRouter');
const cors = require('cors');
const authRouter  = require('./Router/AuthRouter');

const connectDB = require('./Services/ConnectDBService');
require('dotenv').config();

const port = process.env.PORT;

//middlware apply cors and all request
app.use(cors());
// middware get info from client by req.body
app.use(express.json())

connectDB();

app.use('/auth/admin', userRouter);
app.use('/api/auth', authRouter);


app.get('/', function(req,res){
  res.send('test 1');
});

app.get(`/default`,function(req,res){
  res.send('test 2022');
});

app.listen(port, function(){
  console.log(`server is running ${port}`);
});