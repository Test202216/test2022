const UserModel = require('../Models/UserModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const register = async (req, res) => {
    try {
        
        // get info from client
        const { Email, password, role, Name } = req.body;
        // create data to database
        await UserModel.create({
            Name: Name,
            password: bcrypt.hashSync(password, 10),
            Email: Email,
            role: role
        });
        return res.status(200).send('register user finished');
    }
    catch (error) {
        console.log('error', error);
    }
};

const login = async (req, res) => {
    debugger
    const user = await UserModel.findOne({ Email: req.body.Email });
    if (!user) {
        console.log('Invalid Email Or Password');
        return res.status(400).send('Invalid Email Or Password');
    }
    const isPassword = bcrypt.compareSync(req.body.password, user.password);
    if (!isPassword) {
        console.log('Invalid Email Or Password');
        return res.status(400).send('Invalid Email Or Password');
    }
    const jwtToken = jwt.sign({
        _id: user.id,
        Name: user.Name,
        role: user.role
    },'jwtSecret', {
        expiresIn: 60
    });
    return res.status(200).send({
        accessToken: jwtToken
    });
};

module.exports = {
    register: register,
    login: login
};