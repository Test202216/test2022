const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const UserModel = require('../Models/UserModel');

/* const getListUser = async (req, res) => {
    const bearerHeader = req.headers['authorization'];
    const accessToken = bearerHeader.split(' ')[1];

    try {
        // verify token
        const decodejwt = jwt.verify(accessToken, process.env.SECRET_JWT);
        if(decodejwt){
            const users = await UserModel.find();
            res.status(200).send(users);
        }
    } catch (error) {
        if(error instanceof jwt.TokenExpiredError){
            return res.status(401).send('Token Expried');
        }
    }
} */

const getListUser = async (req, res) => {
    
    try {
        const users = await UserModel.find();
        res.status(200).send(users);
    } catch (error) {
        if(error instanceof jwt.TokenExpiredError){
            return res.status(401).send('Token Expried');
        }
    }
}

const postUser = (req, res) => {
    const bearerHeader = req.headers['authorization'];
    const accessToken = bearerHeader.split(' ')[1];

    try {
        // verify token
        const decodejwt = jwt.verify(accessToken, process.env.SECRET_JWT);
        if(decodejwt && decodejwt.role === 'admin'){
            // Save data to user collectiom
            const { Email, password, role, Name } = req.body;
            UserModel.create({
                Name: Name,
                Email: Email,
                password: bcrypt.hashSync(password,10),
                role: role
            });
            res.status(200).send('Create user success !');
        }
    } catch (error) {
        if(error instanceof jwt.TokenExpiredError){
            return res.status(401).send('Token Expried');
        }
    }
}

module.exports = {
    getListUser: getListUser,
    /* userDetail: userDetail */
    postUser: postUser
}