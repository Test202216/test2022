const mongoose =  require('mongoose');

const UserSchema = mongoose.Schema({
    Name: String,
    Email: {
        type: String,
        unique: true
    },
    role: String,
    password: String
});

//Complier
const UserModel = mongoose.model('UserAccount',UserSchema);

module.exports = UserModel;