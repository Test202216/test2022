const mongoose = require('mongoose');
require('dotenv').config();
async function connectDatabase(){
    try{
        await mongoose.connect('mongodb://127.0.0.1:27017/MongoDBAndNodeJSProject');
        console.log('connect database success');
    }catch(error){
        console.log('connect Database Fail',error);
    }
}

module.exports = connectDatabase;